package main

import (
	"log"
	"net/http"

	"gitlab.com/andres07a/golang-examples/api-rest/handler"
)

func main() {
	r := handler.NewRouter()

	server := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	log.Println("Listening...")
	server.ListenAndServe()

}
