package database

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Producto struct {
	ID                  bson.ObjectId `bson:"_id,omitempty"`
	Nombre, Descripcion string
	FechaEntrada        time.Time
}
