package database

import mgo "gopkg.in/mgo.v2"

var session *mgo.Session

type DataStore struct {
	session *mgo.Session
}

//Close mgo.Session
func (d *DataStore) Close() {
	d.session.Close()
}

//Crea un nuevo objeto DataStore para cada solicitud HTTP
func NewDataStore() *DataStore {
	ds := &DataStore{
		session: session.Copy(),
	}
	return ds
}

func init() {

	var err error
	session, err = mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

}
