package handler

import (
	"net/http"

	"github.com/gorilla/mux"
)

// NewRouter retorna las rutas
func NewRouter() http.Handler {

	r := mux.NewRouter()
	//crear controller para utilizar sus metodos
	controller := &controller{}

	r.Handle("/favicon.ico", http.NotFoundHandler())

	r.HandleFunc("/", controller.index)

	return r
}
